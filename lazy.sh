#/bin/bash
#
#
IOS_VERSION = $(ideviceinfo | grep ProductVersion: | cut -d " " -f2)

INSTALLED_APP = $(ideviceinstaller -l | cut -d "," -f1 | tail +2
while [[ ! -z "$INSTALLED_APP" ]]
do
ideviceinstaller -U $(ideviceinstaller -l | cut -d "," -f1 | tail +2)
	INSTALLED_APP = $(ideviceinstaller -l | cut -d "," -f1 | tail +2
done
#	reinstall stock base on version 
idevicediagnostics restart
cd /tmp
case $IOS_VERSION in
	11.0.3)
	curl '"$1"'/ios11-rasputin-disabel.ipa -o jb.ipa
	;;
	12.4)
	curl '"$1"'/ios11-rasputin-disabel.ipa -o jb.ipa
	;;
	13.3)
	curl '"$1"'/ios11-rasputin-disabel.ipa -o jb.ipa
	;;
	13.5)
	curl '"$1"'/ios11-rasputin-disabel.ipa -o jb.ipa
	;;
	14.2)
	curl '"$1"'/ios11-rasputin-disabel.ipa -o jb.ipa
	;;
	*)
	echo "Unexpected verion found.
	exit 2
	;;
esac
ideviceinstaller -i jb.ipa
echo "Ready to provision"
